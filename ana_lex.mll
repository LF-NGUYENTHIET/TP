(* File ana_lex.mll Le type
des tokens est defini dans ana_synt.mli *)
{
open Ana_synt;;

}
let chiffre = ['0'-'9']
let lettre = ['A'-'Z']
let sep = [' ''\t''\n']

rule token_suivant = parse
| chiffre+ { ENTIER(int_of_string (Lexing.lexeme lexbuf)) }
| '+' { PLUS }
| '-' { MINUS }
| sep+ { token_suivant lexbuf }
| _ { failwith "caractere non reconnu" }
| eof { FIN }


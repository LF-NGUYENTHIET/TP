/* File ana_synt.mly */
%{
open Express;;
%}
/* liste des tokens */
%token <int> ENTIER
%token PLUS MINUS FIN
/* l’axiome, et son type */
%start entree
%type <Express.expr> entree
%%
/* liste des regles */
entree:
    expr FIN { $1 } ;
expr:
    expr PLUS expr { Add($1,$3) }
    |expr MINUS expr { Sub($1,$3) }
    | ENTIER { Cte($1) }
    ;

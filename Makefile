all:
	ocamlyacc ana_synt.mly
	ocamllex ana_lex.mll
	ocamlc -c express.mli
	ocamlc -c express.ml
	ocamlc -c ana_synt.mli
	ocamlc -c ana_synt.ml
	ocamlc -c ana_lex.ml
	ocamlc -c test_expr.ml
	ocamlc -o test_expr express.cmo ana_synt.cmo ana_lex.cmo test_expr.cmo

clean:
	rm -rf *.cm*
	rm -rf ana_lex.ml
	rm -rf ana_synt.mli
	rm -rf ana_synt.ml
	rm -rf test_expr
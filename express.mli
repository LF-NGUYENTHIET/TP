(* File EXPRESS.mli *)
type expr =
    Cte of int
  | Add of expr * expr
  | Sub of expr * expr;;
val print_expr : expr -> unit;;

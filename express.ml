(* File express.ml *)
type expr =
    Cte of int
  | Add of expr * expr
  | Sub of expr * expr;;

let rec print_expr e= match e with
  |Cte n->print_int n
  |Add (e1,e2)-> print_string "[+ , ";
    print_expr e1;print_string " ; ";
    print_expr e2;print_string " ]"
  |Sub (e1,e2)-> print_string "[- , ";
    print_expr e1;print_string " ; ";
    print_expr e2; print_string " ]";;

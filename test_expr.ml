(* File test_expr.ml *)
let expr_of_string s =
  let buf = Lexing.from_string s
  in Ana_synt.entree Ana_lex.token_suivant buf
;;
let test s =
  try
    let e = expr_of_string s
    in begin
      Express.print_expr e;

      print_newline ()
    end
  with Parsing.Parse_error ->
    begin
      print_string "Erreur de syntaxe";
      print_newline ()
    end
     | Failure "caractere non reconnu" ->
       begin
         print_string "Caractere non reconnu";
         print_newline ()
       end
;;
let main () =
  begin
    test "1-2-3";test "1*2/3";
    test "1+2*3";test "(1+2)*3";
    test "[A<-1][B<-3]1+B+A";
    test "1+"; test "tout faux";
  end
;;
Printexc.catch main ();;
